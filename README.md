# Sandbag

## 使用說明書
### Server
1. 環境佈置
    * AMI : Amazon Linux AMI 2018.03.0 (HVM), SSD Volume Type 
    * Region : US East (N. Virginia)
    * VPC : 隨意
    * SG : port 80
2. <b>user data </b>
    * Version_v1_EC2
        * > #!/bin/bash <br>
            sudo yum update -y <br>
            sudo yum install git -y <br>
            sudo pip install flask <br>
            cd home/ec2-user  <br>
            sudo git clone https://gitlab.com/asdf024681029/hank_test.git <br>
            sudo python hank_test/server/server_v1_EC2.py <br>
    
    * Version _v2_EC2
        * > #!/bin/bash <br>
            sudo yum update -y <br>
            sudo yum install git -y <br>
            sudo pip install flask <br>
            cd home/ec2-user  <br>
            sudo git clone https://gitlab.com/asdf024681029/hank_test.git <br>
            sudo python hank_test/server/server_v2_EC2.py <br>


    * Version _v2_ECS
        * > #!/bin/bash <br>
            sudo yum update -y <br>
            sudo yum install git -y <br>
            sudo pip install flask <br>
            cd home/ec2-user  <br>
            sudo git clone https://gitlab.com/asdf024681029/hank_test.git <br> 
            sudo python hank_test/server/server_v2_ECS.py<br>
    


    
        
### Server_RDS
1. 環境佈置   
    - rds 
        - port : 3306 
        - public access : yes
        - MySQL 5.7.22
        - Free tier
        - VPC : 隨意
        - Publicly accessible : Yes
            - user : admin
            - password : 123456789
        - 啟用RDS後,記得要create sites, insert value -> otherwise DB will get error, 詳細資訊可以執行DB file 
    - Redis
        - name : hank
        - VPC : hank
        - t2.mirco
        - !! 如果要用其他請修改client.py and scoreboard.py and server.py 的 endpoint 
    - EC2 
        - AMI : Amazon Linux AMI 2018.03.0 (HVM), SSD Volume Type 
        - Region : US East (N. Virginia)
        - VPC : 跟ElastiCache一樣VPC
        - SG : port 80

2. user data
    * Version _v2_EC2_DB
        * > #!/bin/bash <br>
            sudo yum update -y <br>
            sudo yum install git -y <br>
            sudo pip install flask <br>
            sudo pip install mysql-connector <br>
            cd home/ec2-user  <br>
            sudo git clone https://gitlab.com/asdf024681029/hank_test.git <br> 
            sudo python hank_test/server/server_rds_EC2.py<br>



### Server_DynamoDB
1. 環境佈置   
    * 環境佈置：
        - DynamoDB
            - Name : sandbag
            - primary : Id(String)
        - Redis
            - name : hank
            - VPC : hank
            - t2.mirco
            - 如果要用其他請修改client.py and scoreboard.py and server_DynamoDB.py 的 endpoint 
        - EC2 
            - AMI : Amazon Linux AMI 2018.03.0 (HVM), SSD Volume Type 
            - Region : US East (N. Virginia)
            - VPC : 跟ElastiCache一樣VPC
            - SG : port 80
            - use role (DynamoDB)
            
2. user data
    * Version _v2_ECS_DB
        * > #!/bin/bash <br>
            sudo yum update -y <br>
            sudo yum install git -y <br>
            sudo pip install flask <br>
            sudo pip install boto3 <br>
            cd home/ec2-user  <br>
            sudo git clone https://gitlab.com/asdf024681029/hank_test.git <br> 
            sudo python hank_test/server/server_dydb.py<br>

### Client 
1. 環境佈置
    * 先啟用ElastiCache -- VPC (name : hank), 如果要用其他請修改client.py and scoreboard.py 的 endpoint 
    * AMI : Amazon Linux AMI 2018.03.0 (HVM), SSD Volume Type 
    * Region : US East (N. Virginia)
    * EC2 - VPC : 跟ElastiCache 選擇的VPC ㄧ樣
    * SG : port 80

2. <b>user data </b>
    * > #!/bin/bash <br>
        sudo yum update -y <br>
        sudo yum install git -y <br>
        sudo pip install flask <br>
        sudo pip install redis <br>
        cd home/ec2-user  <br>
        sudo git clone https://gitlab.com/asdf024681029/hank_test.git <br>
        sudo python hank_test/client/client.py <br>
    
3. <b>需要修改地方</b>
    * 希望可以在一台EC2上面開啟兩個以上的python 檔案, 比較省錢
    * 可不可以使用連線不中斷的方式->達到DDOS




### Client_DB
1. 環境佈置
    * 先啟用ElastiCache -- VPC (name : hank), 如果要用其他請修改client.py and scoreboard.py 的 endpoint 
    * AMI : Amazon Linux AMI 2018.03.0 (HVM), SSD Volume Type 
    * Region : US East (N. Virginia)
    * EC2 - VPC : 跟ElastiCache 選擇的VPC ㄧ樣
    * SG : port 80

2. <b>user data </b>
    * > #!/bin/bash <br>
        sudo yum update -y <br>
        sudo yum install git -y <br>
        sudo pip install flask <br>
        sudo pip install redis <br>
        cd home/ec2-user  <br>
        sudo git clone https://gitlab.com/asdf024681029/hank_test.git <br>
        sudo python hank_test/client/client_rds.py <br>
3. <b>user data </b>
    * > #!/bin/bash <br>
        sudo yum update -y <br>
        sudo yum install git -y <br>
        sudo pip install flask <br>
        sudo pip install redis <br>
        cd home/ec2-user  <br>
        sudo git clone https://gitlab.com/asdf024681029/hank_test.git <br>
        sudo python hank_test/client/client_dydb.py <br>

### Client_multi
1. 環境佈置
    * 先啟用ElastiCache -- VPC (name : hank), 如果要用其他請修改client.py and scoreboard.py 的 endpoint 
    * AMI : Amazon Linux AMI 2018.03.0 (HVM), SSD Volume Type 
    * Region : US East (N. Virginia)
    * EC2 - VPC : 跟ElastiCache 選擇的VPC ㄧ樣
    * SG : port 80

2. <b>user data </b>
    * > #!/bin/bash <br>
        sudo yum update -y<br>
        sudo yum install git -y<br>
        cd home/ec2-user <br>
        sudo git clone https://gitlab.com/asdf024681029/hank_test.git<br>
        sudo yum install python34 -y<br>
        sudo yum install python34-pip -y<br>
        sudo /usr/bin/pip-3.4 install redis<br>
        sudo /usr/bin/pip-3.4 install flask<br>
        sudo /usr/bin/pip-3.4 install boto3<br>
        sudo /usr/bin/pip-3.4 install requests<br>
        sudo python3 hank_test/client/client_multi.py <br>
    
3. <b>需要修改地方</b>
    * 希望可以在一台EC2上面開啟兩個以上的python 檔案, 比較省錢
    * 可不可以使用連線不中斷的方式->達到DDOS

### Scoreboard 

1. 環境佈置
    * 要先啟用ElastiCache -- VPC (name : hank), 如果要用其他請修改client.py and scoreboard.py 的 endpoint 
    * AMI : Amazon Linux AMI 2018.03.0 (HVM), SSD Volume Type 
    * Region : US East (N. Virginia)
    * EC2 - VPC : 要跟ElastiCache 選擇的VPC ㄧ樣
    * SG : port 80
    * 請用role (translate 的role)

2. <b>user data </b>
    * > #!/bin/bash <br>
        sudo yum update -y<br>
        sudo yum install git -y<br>
        cd home/ec2-user <br>
        sudo git clone https://gitlab.com/asdf024681029/hank_test.git<br>
        sudo yum install python34 -y<br>
        sudo yum install python34-pip -y<br>
        sudo /usr/bin/pip-3.4 install redis<br>
        sudo /usr/bin/pip-3.4 install flask<br>
        sudo /usr/bin/pip-3.4 install boto3<br>
        sudo /usr/bin/pip-3.4 install requests<br>
        sudo python3 hank_test/Scoreboard/scoreboard.py<br>

### Scoreboard_RDS

1. 環境佈置
    * 要先啟用ElastiCache -- VPC (name : hank), 如果要用其他請修改client.py and scoreboard.py 的 endpoint 
    * AMI : Amazon Linux AMI 2018.03.0 (HVM), SSD Volume Type 
    * Region : US East (N. Virginia)
    * EC2 - VPC : 要跟ElastiCache 選擇的VPC ㄧ樣
    * SG : port 80
    * 請用role (translate 的role)

2. <b>user data _ DB版本 </b>
    * > #!/bin/bash <br>
        sudo yum update -y<br>
        sudo yum install git -y<br>
        cd home/ec2-user <br>
        sudo git clone https://gitlab.com/asdf024681029/hank_test.git<br>
        sudo yum install python34 -y<br>
        sudo yum install python34-pip -y<br>
        sudo /usr/bin/pip-3.4 install redis<br>
        sudo /usr/bin/pip-3.4 install flask<br>
        sudo /usr/bin/pip-3.4 install boto3<br>
        sudo /usr/bin/pip-3.4 install requests<br>
        sudo python3 hank_test/Scoreboard/scoreboard_rds.py<br>


### Scoreboard_DynamoDB

1. 環境佈置
    * 要先啟用ElastiCache -- VPC (name : hank), 如果要用其他請修改client.py and scoreboard.py 的 endpoint 
    * AMI : Amazon Linux AMI 2018.03.0 (HVM), SSD Volume Type 
    * Region : US East (N. Virginia)
    * EC2 - VPC : 要跟ElastiCache 選擇的VPC ㄧ樣
    * SG : port 80
    * 請用role (translate and DynamoDB )

2. <b>user data _ DYDB版本 </b>
    * > #!/bin/bash <br>
        sudo yum update -y<br>
        sudo yum install git -y<br>
        cd home/ec2-user <br>
        sudo git clone https://gitlab.com/asdf024681029/hank_test.git<br>
        sudo yum install python34 -y<br>
        sudo yum install python34-pip -y<br>
        sudo /usr/bin/pip-3.4 install redis<br>
        sudo /usr/bin/pip-3.4 install flask<br>
        sudo /usr/bin/pip-3.4 install boto3<br>
        sudo /usr/bin/pip-3.4 install requests<br>
        sudo python3 hank_test/Scoreboard/scoreboard_dydb.py<br>

### Scoreboard_multi

1. 環境佈置
    * 要先啟用ElastiCache -- VPC (name : hank), 如果要用其他請修改client.py and scoreboard.py 的 endpoint 
    * AMI : Amazon Linux AMI 2018.03.0 (HVM), SSD Volume Type 
    * Region : US East (N. Virginia)
    * EC2 - VPC : 要跟ElastiCache 選擇的VPC ㄧ樣
    * SG : port 80
    * 請用role (translate and DynamoDB )

2. <b>user data _ DYDB版本 </b>
    * > #!/bin/bash <br>
        sudo yum update -y<br>
        sudo yum install git -y<br>
        cd home/ec2-user <br>
        sudo git clone https://gitlab.com/asdf024681029/hank_test.git<br>
        sudo yum install python34 -y<br>
        sudo yum install python34-pip -y<br>
        sudo /usr/bin/pip-3.4 install redis<br>
        sudo /usr/bin/pip-3.4 install flask<br>
        sudo /usr/bin/pip-3.4 install boto3<br>
        sudo /usr/bin/pip-3.4 install requests<br>
        sudo python3 hank_test/Scoreboard/scoreboard_multi.py<br>