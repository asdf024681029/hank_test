from flask import Flask,render_template,redirect,session,Blueprint
from flask import Response
from flask import request,jsonify
import json
import redis
import boto3
import requests

app = Flask(__name__)
r=redis.Redis(host='hank-001.bwwxt6.0001.use1.cache.amazonaws.com', port=6379) # read only                                                    
r1=redis.Redis(host='hank-003.bwwxt6.0001.use1.cache.amazonaws.com', port=6379) # read only                                                   
client = boto3.client('translate',region_name="us-east-1")


@app.route('/_stuff')
def add_numbers():
    username = session.get('username','username')
    score=str(r1.get(username+"_score"),encoding = "utf-8")
    if(score==None):
        score="None"
    return jsonify(result=score)

@app.route('/_admin')
def all_score_admin():
    username_all_score={}
    for key in r1.scan_iter():
        key=str(key, encoding = "utf-8")
        if("score" in key):
            user_key=key.replace("_score",  "")
            username_all_score[user_key]=str(r1.get(key),encoding = "utf-8")
    print(username_all_score)
    return jsonify(result=username_all_score)


@app.route('/')
def login():
    return render_template("login.html")


@app.route('/login', methods=['POST'])
def login_1():
    session['username'] = request.form['login']
    session.permanent = True
    username=session.get('username','username')
    if(username=='ADMIN'):
        return redirect('/admin')
    return redirect('/index')

@app.route('/admin')
def admin():
    return render_template("admin.html")

@app.route('/index')
def index():
    username = session.get('username','username')
    endpoint=""
    endpoint_translate=""
    username_all=[]
    try:
        endpoint=r1.get(username+"_endpoint")
    except:
        print("no endpoint")
    try:
        endpoint_translate = session.get('endpoint_translate','endpoint_translate')
    except:
        print("no endpoint")
    try:
        for key in r.scan_iter():
            key=str(key, encoding = "utf-8")
            if("endpoint" in key):
                user_key=key.replace("_endpoint", "")
                username_all.append(user_key)
    except:
        print("no username")
    return render_template("index_multi.html",username=username,endpoint=endpoint,endpoint_translate=endpoint_translate, len = len(username_all), username_all = username_all)

@app.route('/endpoint', methods=['POST'])
def endpoint():
    username = session.get('username','username')
    text = request.form['text']
    r.set(username+"_endpoint",text)
    return redirect('/index')

@app.route('/endpoint_translate', methods=['POST'])
def endpoint1():
    text = request.form['endpoint_translate']
    my_data = {
    "content": "White House officials blocked reporters and their cameras from entering the two hospitals during his visits to Ohio and Texas\
 this week, a move they said was out of respect for the patients' privacy. But according to one person familiar with the President's reaction\
, the President lashed out at his staff for keeping the cameras away from him, complaining that he wasn't receiving enough credit. Aides had \
feared a moment like the one that is now going viral-where the President appears to focus on himself in front of those still recovering from \
a tragedy.",
    "source-lanuage": "en",
    "target-lanuage": "fr"
    }
    print(text)
    #r = requests.get(text, data = my_data)                                                                                                   
    endpoint_translate=""
    try :
        rn = requests.post(text, json = my_data)
        #print(r)                                                                                                                             
        result = json.loads(rn.text)
        #print(result)                                                                                                                        
        response = client.translate_text(
            Text=my_data['content'],
            SourceLanguageCode=my_data['source-lanuage'],
            TargetLanguageCode=my_data['target-lanuage']
        )
        answer = response['TranslatedText']
        if answer == result['translate-content']:
            endpoint_translate='Your API is working perfectly.'
        else :
            endpoint_translate='Your API is not working.'

    except Exception as e:
        endpoint_translate='Your API is not working.' + str(e)
    session['endpoint_translate'] = endpoint_translate
    session.permanent = True
    return redirect('/index')

@app.route('/reset', methods=['POST'])
def reset():
    text = request.form['reset']
    for key in r.scan_iter():
        r.delete(key)
    return render_template('index_multi.html')



if __name__ == "__main__":
    app.secret_key = "super secret key"
    app.run(host='0.0.0.0', port=80)
