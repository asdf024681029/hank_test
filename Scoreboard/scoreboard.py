from flask import Flask,render_template
from flask import Response
from flask import request,jsonify
import json
import redis
import boto3
import requests
  
app = Flask(__name__)
r=redis.Redis(host='hank-001.bwwxt6.0001.use1.cache.amazonaws.com', port=6379) # read only
r1=redis.Redis(host='hank-003.bwwxt6.0001.use1.cache.amazonaws.com', port=6379) # read only
client = boto3.client('translate',region_name="us-east-1")



@app.route('/_stuff')
def add_numbers():
    score=int(r1.get("score"))
    if(score==None):
        score="None"
    return jsonify(result=score)



@app.route('/')
def index():
    return render_template("index.html")


@app.route('/endpoint', methods=['POST'])
def endpoint():
    text = request.form['text']
    r.set("endpoint",text)
    endpoint=r1.get("endpoint")
    return render_template('index.html',endpoint=endpoint)


@app.route('/endpoint_translate', methods=['POST'])
def endpoint1():
    text = request.form['endpoint_translate']
    my_data = {
    "content": "White House officials blocked reporters and their cameras from entering the two hospitals during his visits to Ohio and Texas this week, a move they said was out of respect for the patients' privacy. But according to one person familiar with the President's reaction, the President lashed out at his staff for keeping the cameras away from him, complaining that he wasn't receiving enough credit. Aides had feared a moment like the one that is now going viral-where the President appears to focus on himself in front of those still recovering from a tragedy.",
    "source-lanuage": "en",
    "target-lanuage": "fr"
    }
    print(text)
    #r = requests.get(text, data = my_data)
    try :
        rn = requests.post(text, json = my_data)
        #print(r)
        result = json.loads(rn.text)
        #print(result)
        response = client.translate_text(
            Text=my_data['content'],
            SourceLanguageCode=my_data['source-lanuage'],
            TargetLanguageCode=my_data['target-lanuage']
        )
        endpoint_translate=""
        #print("TranslatedText:",response['TranslatedText'])
        answer = response['TranslatedText']
        if answer == result['translate-content']:
            endpoint_translate='Your API is working perfectly.'
        else :
            endpoint_translate='Your API is not working.'
    except Exception as e:
        endpoint_translate='Your API is not working.' + str(e)
    return render_template('index.html',endpoint_translate=endpoint_translate)    

@app.route('/reset', methods=['POST'])
def reset():
    text = request.form['reset']
    for key in r.scan_iter():
        r.delete(key)
    return render_template('index.html')



if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
