import optparse
import time
import threading #modules importation
import requests
import redis
import socket
import redis
import uuid



# for version time 
time_a="2019-8-8 6:30"
time_a_1 = time.strptime(time_a, "%Y-%m-%d %H:%M")
version_time = int(time.mktime(time_a_1))

# for type time 
time_b="2019-8-13 6:30"
time_b_1 = time.strptime(time_b, "%Y-%m-%d %H:%M")
from_type_time= int(time.mktime(time_b_1))


# connect redis
r = redis.Redis(host='hank-001.bwwxt6.0001.use1.cache.amazonaws.com', port=6379)
r1 = redis.Redis(host='hank-003.bwwxt6.0001.use1.cache.amazonaws.com', port=6379) 

# init variable 
ip = socket.gethostbyname(socket.gethostname())
last_time = 0




           
def stresser(): # stresser function
    global ip   
    global score  
    global version_time
    global from_type_time
    global last_time
    global endpoint
    index=0
    endpoint={}
    username=[]
    while (1 < 4) : 
        # create unique token in header 
        random = str(uuid.uuid4()) 
        req_header={'Name': random}

        name=""
        version=""
        from_type=""
        score=0
        now_time=time.time()

        # get endpoint
        if (time.time() - last_time) > 5:
            last_time = time.time()
            username=[]
            for key in r1.scan_iter():
                key=str(key, encoding = "utf-8")
                if("endpoint" in key):
                    user_key=key.replace("_endpoint", "")
                    username.append(user_key)
                    endpoint_value=str(r1.get(key), encoding = "utf-8")
                    if(endpoint_value.startswith("http")):
                        print("")
                        endpoint[user_key]=endpoint_value
                    else:
                        endpoint_value="http://"+endpoint_value
                        endpoint[user_key]=endpoint_value
                else:
                    print("no")
        print(username)
                
        user=username[index]
        # get header 
        try:
            response=requests.get(endpoint[user],headers=req_header,timeout=0.01) # sending requests
            name=response.headers['Name']
            version=response.headers['Version']
            from_type=response.headers['From']
        except:
            score-=30
        # judge name
        if(name==random):
            score+=50
        else:
            score-=50

        # judge version
        if(now_time<version_time):
            if(version=='v1'):
                score+=10
                
            else:
                score-=10
        else:
            if(version=='v2'):
                score+=20
            else:
                score-=20
            
        # judge type
        if(now_time < from_type_time):
            if(from_type=='EC2'):
                score+=10  
            else:
                score-=10
        else:
            if(from_type=='ECS'):
                score+=20
            else:
                score-=20

        r.incr(user+'_score',score)
        print("score is ",score)
        print("Name is ",name)
        print("Version is ",version)
        print("Type is ",from_type)
        print("Endpoint is ",endpoint[user])
        print(user)
        index+=1
        if(index>=len(endpoint)):
            index=0
 


def _threads_(): # threading function
    c= threading.Thread(target=stresser) #creating threads
    d= threading.Thread(target=stresser)
    a= threading.Thread(target=stresser)
    e= threading.Thread(target=stresser)
    z= threading.Thread(target=stresser)
    x= threading.Thread(target=stresser)
    c1= threading.Thread(target=stresser)
    d1= threading.Thread(target=stresser)
    a1= threading.Thread(target=stresser)
    e1= threading.Thread(target=stresser)
    z1= threading.Thread(target=stresser)
    x1= threading.Thread(target=stresser)
    c.start() # starting threads
    c.join()
    d.start()
    d.join()
    a.start()
    a.join()
    e.start()
    e.join()
    z.start()
    z.join()
    x.start()
    x.join()
    c1.start()
    c1.join()
    d1.start()
    d1.join()
    a1.start()
    a1.join()
    e1.start()
    e1.join()
    z1.start()
    z1.join()
    x1.start()
    x1.join()


	 
	
def main():
	print ("[*] start flooding ")
	time.sleep(1)
	_threads_() 

main()
