from flask import Flask,render_template
from flask import Response,request,jsonify
import json

app = Flask(__name__)


name=""
count=0
@app.after_request
def apply_caching(response):
    #print(response)
    global name
    response.headers["Name"] = name
    response.headers["Version"] = "v2"
    response.headers["From"] = "ECS"
    return response
    
@app.route('/', methods=['GET', 'POST'])
def home():
    global name
    global count
    try :
        count+=1
        print("test:")
        name =request.headers['Name']
        print(name)
    except:
        print("error")
    return render_template('server_test.html')


@app.route('/_count')
def add_numbers():
    global count
    return jsonify(result=count)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
